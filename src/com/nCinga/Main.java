package com.nCinga;

import com.nCinga.bo.Account;
import com.nCinga.bo.Transaction;
import com.nCinga.enums.TransactionType;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Account account1 = new Account(123, 500);

        Transaction transaction1 = new Transaction(account1, 100, TransactionType.DEPOSIT);
        Thread t1 = new Thread(transaction1);

        Transaction transaction2 = new Transaction(account1, 200, TransactionType.WITHDRAW);
        Thread t2 = new Thread(transaction2);

        Transaction transaction3 = new Transaction(account1, 300, TransactionType.DEPOSIT);
        Thread t3 = new Thread(transaction3);

        Transaction transaction4 = new Transaction(account1, 50, TransactionType.WITHDRAW);
        Thread t4 = new Thread(transaction4);

        Transaction transaction5 = new Transaction(account1, 200, TransactionType.DEPOSIT);
        Thread t5 = new Thread(transaction5);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();

        Thread.sleep(2000);

        System.out.print("ACCOUNT BALANCE AFTER TRANSACTIONS: ");
        account1.displayAccBalance();

    }
}

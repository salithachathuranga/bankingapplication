package com.nCinga.bo;

public class Account {

    private final int accId;
    private double accBalance;

    public Account(int accId, double accBalance) {
        this.accId = accId;
        this.accBalance = accBalance;
    }

    public int getAccId() {
        return accId;
    }

    public double getAccBalance() {
        return accBalance;
    }

    public void displayAccBalance() {
        System.out.println(getAccBalance());
    }

    public void setAccBalance(double accBalance) {
        this.accBalance = accBalance;
    }

    public synchronized void deposit(double transactAmount) {
        this.setAccBalance(this.getAccBalance() + transactAmount);
        System.out.println("You have deposited "+transactAmount+" USD => Your Account Balance: "+accBalance);
        System.out.println();
    }

    public synchronized void withdraw(double transactAmount) {
        this.setAccBalance(this.getAccBalance() - transactAmount);
        System.out.println("You have withdrawn "+transactAmount+" USD => Your Account Balance: "+accBalance);
        System.out.println();
    }

}

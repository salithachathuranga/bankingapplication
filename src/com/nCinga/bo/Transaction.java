package com.nCinga.bo;

import com.nCinga.enums.TransactionType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Transaction implements Runnable{

    private int transactId;
    private static int autoIncId = 0;
    private String transactDate;
    private double transactAmount;
    private Account transactAccount;
    private TransactionType transactType;

    public Transaction(Account transactAccount, double transactAmount, TransactionType transactType) {
        this.transactId = autoIncId();
        this.transactDate = setTransactDate();
        this.transactAccount = transactAccount;
        this.transactAmount = transactAmount;
        this.transactType = transactType;
    }

    public Account getTransactAccount() {
        return transactAccount;
    }

    public TransactionType getTransactType() {
        return transactType;
    }

    public int getTransactId() {
        return transactId;
    }

    public String getTransactDate() {
        return transactDate;
    }

    public double getTransactAmount() {
        return transactAmount;
    }

    private int autoIncId(){
        return ++autoIncId;
    }

    private String setTransactDate(){
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(date);
    }

    public void depositMoney(double transactAmount) {
        transactAccount.deposit(transactAmount);
    }

    public void withdrawMoney(double transactAmount) {
        transactAccount.withdraw(transactAmount);
    }

    public void run() {
        if(transactType.equals(TransactionType.DEPOSIT)) {
            depositMoney(getTransactAmount());
        }
        else {
            withdrawMoney(getTransactAmount());
        }
    }
}

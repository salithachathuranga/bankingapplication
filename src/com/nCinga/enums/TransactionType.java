package com.nCinga.enums;

public enum TransactionType {
    DEPOSIT,
    WITHDRAW
}
